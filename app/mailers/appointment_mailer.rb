class AppointmentMailer < ApplicationMailer
	default from: "claire.bayoda@jumpdigital.asia"

	def welcome_email(current_user)
		@user = current_user
		@url = "localhost:3000"
		mail(to: current_user.email, subject: "Thanks for Booking with us!")
	end
end
