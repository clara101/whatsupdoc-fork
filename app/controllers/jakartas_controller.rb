class JakartasController < ApplicationController

	def index
		@hospitals = Hospital.all.order("created_at ASC")
		@hospital1 = Hospital.where(:categ => 1)

		@hospital2 = Hospital.where(:categ => 2)

		@hospital3 = Hospital.where(:categ => 3)

		@hospital4 = Hospital.where(:categ => 4)

		@hospital5 = Hospital.where(:categ => 5)

		@categs = Category.all

	end



	def new
		@hospital = Hospital.new
	end

	def create
		@hospital = Hospital.new(hospital_params)
	end

	private

	def hospital_params
		params.require(:hospital).permit(:name, :address, :telphone, :fax, :categ)
	end


end
