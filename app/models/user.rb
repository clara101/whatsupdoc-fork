class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :omniauthable, :validatable
         
  validates :email, presence: true
  validates :email, uniqueness: true
  validates :password, length: { minimum: 8 }
  validates :password, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  #validates :city, presence: true
  #validates :gender, presence: true
  #validates :address, presence: true

  has_many :doctors
  has_many :reviews

  # method that interacts with Omniauth object

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.name = auth.info.name
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
      user.role = "guest"
      user.password = Devise.friendly_token[0,20]
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end

  def self.find_for_google_oauth(access_token)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    # Uncomment the section below if you want users to be created if they don't exist
     unless user
         user = User.create(name: data["name"],
            provider:access_token.provider,
            uid: access_token.uid,
            first_name: data["first_name"],
            last_name: data["last_name"],
            email: data["email"],
            password: Devise.friendly_token[0,20],
            role: "guest"
         )
     end
    user
end

  RailsAdmin.config do |config|
    config.model 'User' do

      # Create fields
      create do
        field :email
        field :first_name
        field :last_name
        field :name
        field :role
        field :password
      end

      edit do
        field :email
        field :first_name
        field :last_name
        field :name
        field :role
        field :password
      end 

      list do 
        field :first_name
        field :last_name
        field :email
        field :provider
        field :sign_in_count
        field :uid
        field :name
        field :last_sign_in_at

      end

    end
  end


end
