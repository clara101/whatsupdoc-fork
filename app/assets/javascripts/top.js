$(function(){
	// Add margin-top on every ul after a Paragraph
	$('.article-single p').each(function() {
		if ($(this).siblings().size() > 0) {
			$(this).nextAll('ul').first().css('margin-top', '14px');
			$(this).nextAll('ol').first().css('margin-top', '14px');
		}
	});

	// Add margin-top on every p after a Listing
	$('.article-single ul').each(function() {
		if ($(this).siblings().size() > 0) {
			$(this).nextAll('p').first().css('margin-top', '14px');
		}
	});
});