# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: 'admin@example.com', password: 'password', role:'admin')
User.create(email: 'guest@example.com', password: 'password', role:'guest')
User.create(email: 'doctor@example.com', password: 'password', role:'doctor')

Category.create([
	{ name: 'Jakarta Barat'},
	{ name: 'Jakarta Pusat'},
	{ name: 'Jakarta Selatan'},
	{ name: 'Jakarta Timur'},
	{ name: 'Jakarta Utara'}
	])

Hospital.create([
		#Jakarta Barat
		{ name: 'RS Royal Taruma', address: 'Jl. Daan Mogot No.34 Grogol', telphone: '56958338', fax: '', categ: '1'},
		{ name: 'RS. Siloam Graha Medika ', address: 'Jl. Raya Perjuangan Kav. 8 Kebon Jeruk Jakarta Barat 11530 ', telphone: '5300888', fax: '5321775', categ: '1'},
		{ name: 'RS. Pelni Petamburan ', address: 'Jl. K. S. Tubun No. 92 - 94 ', telphone: '5306901', fax: '5483145', categ: '1'},
		{ name: 'RSIA. Hermina Daan Mogot ', address: 'Jl. Kintamani Raya No. 2 Perumahan Daan Mogot Baru Jakarta Barat ', telphone: '5408989', fax: '5449869', categ: '1'},
		{ name: 'RS. MH. Thamrin Cengkareng ', address: 'Jl. Daan Mogot Km. 17 Cengkareng ', telphone: '6194581', fax: '6194581', categ: '1'},
		{ name: 'RS. Medika Permata Hijau ', address: 'Jl. Raya Kebayoran Lama No. 64 Jakarta ', telphone: '5305288', fax: '5305291', categ: '1'},
		{ name: 'RS. Sumber Waras ', address: 'Jl. Kyai Tapa No. 1 Grogol Jakarta Barat 11440 ', telphone: '5682011', fax: '5673122', categ: '1'},
		{ name: 'MH Thamrin UPK ANGKE ', address: 'Jl. Tubagus Angke No. 27 Jak - Bar ', telphone: '5675259', fax: '', categ: '1'},
		{ name: 'Klinik MHT Kali Deres ', address: 'Jl. Peta Selatan Raya Kalideres Indah I Blok A No. 2', telphone: '5456027', fax: '', categ: '1'},
		{ name: 'RS. Kanker "DHARMAIS" *', address: 'Jl. Let. Jend. S. Parman Kav. 84 - 86, Jakarta Barat 11420 ', telphone: '5681570', fax: '5681579, 5681577', categ: '1'},
		{ name: 'Klinik "YADIKA" Tegal Alur', address: 'Jl. Kamal Raya A7 No. 7-11, Tegal Alur, Jakarta Barat', telphone: '70605247', fax: '', categ: '1'},
		{ name: 'RS Puri Mandiri Kedoya ', address: 'Jl. Kedoya Raya No.2 Kebun Jeruk', telphone: '5828299', fax: '', categ: '1'},
		{ name: 'Poliklinik Cengkareng Pertamina', address: 'Jl. Daan Mogot Pintu I', telphone: '70602536', fax: '', categ: '1'},
		#Jakarta Pusat
		{ name: 'CHC Clinic ', address: 'Menara Kebon Sirih- Podium I, Jl. Kebon Sirih Kav 17 -19', telphone: '3921323', fax: '', categ: '2'},
		{ name: 'Klinik Dr. Murni Indra MK3 & Associates', address: 'Jl. Proklamasi No. 61A', telphone: '31908203', fax: '', categ: '2'},
		{ name: 'Klinik Kantor Pusat Pertamina', address: 'Jl. Perwira No. 2 - 4', telphone: '3815970', fax: '', categ: '2'},
		{ name: 'Klinik Kwarnas Pertamina', address: 'Jl. Medan Merdeka Timur No. 6', telphone: '3502150 Ext. 1997', fax: '', categ: '2'},
		{ name: 'Klinik Merdeka Timur Pertamina', address: 'Jl. Merdeka Timur No. 12', telphone: '3865415', fax: '', categ: '2'},
		{ name: 'Klinik Rehabilitasi Pertamina ', address: 'Jl. Perwira No. 2 - 4', telphone: '3816699', fax: '', categ: '2'},
		{ name: 'MH Thamrin UPK SERDANG', address: 'Jl. H. Ung E 71 No. 2, Kemayoran', telphone: '4249519', fax: '', categ: '2'},
		{ name: 'Praktek Dokter Bersama & Apotik Yes Care ', address: 'Plaza Kebon Sirih Podium I PI-13 B, Jl. Kebon Sirih Kav 12 -19 ', telphone: '3926976', fax: '', categ: '2'},
		{ name: 'RS Bunda Menteng ', address: 'Jl. Teuku Cik Ditiro No.28 Menteng', telphone: '31922005', fax: '', categ: '2'},
		{ name: 'RS Islam Jakarta ', address: 'Jl. Cempaka Putih Tengah I/II', telphone: '4250451', fax: '', categ: '2'},
		{ name: 'RS. Abdi Waluyo', address: 'Jl. HOS. Tjokroaminoto No. 31-33 Menteng', telphone: '3144989', fax: '330866', categ: '2'},
		{ name: 'RS. Husada ', address: 'Jl. Raya Mangga Besar No. 137 - 139', telphone: '6260108', fax: '6497434', categ: '2'},
		{ name: 'RS. Khusus THT - Bedah PROKLAMASI', address: 'Jl. Proklamasi No. 43 Jakarta Pusat 10320', telphone: '3900002', fax: '3900947', categ: '2'},
		{ name: 'RS. Kramat 128 ', address: 'Jl. Kramat Raya No. 128', telphone: '3909513', fax: '3909125', categ: '2'},
		{ name: 'RS. Menteng Mitra Afia', address: 'Jl. Kalipasir No. 9 Cikini', telphone: '3154050', fax: '3146309', categ: '2'},
		{ name: 'RS. MH. Thamrin Internasional Salemba', address: 'Jl. Salemba Tengah No. 26 -28', telphone: '3904422', fax: '3107816', categ: '2'},
		{ name: 'RS. Pertamina Jaya', address: 'Jl. A. Yani No. 2', telphone: '4211911', fax: '4211913 Ext 4230', categ: '2'},
		{ name: 'RS. PGI Cikini', address: 'Jl. Raden Saleh No. 40', telphone: '324471', fax: '324663', categ: '2'},
		{ name: 'RS. Saint Carolus', address: 'Jl. Salemba Raya No. 41', telphone: '3904441', fax: '3922831', categ: '2'},
		{ name: 'RSB. Budi Kemuliaan ', address: 'Jl. Budi Kemuliaan No. 25 Jakarta 10110', telphone: '3842828', fax: '3501012', categ: '2'},
		{ name: 'RSIA. "TAMBAK" ', address: 'Jl. Tambak No. 18', telphone: '2303444', fax: '3902550', categ: '2'},
		{ name: 'RSPAD Gatot Soebroto "Pav Anak" ', address: 'Jl. Abdul Rachman Saleh No 24 ', telphone: ' 	3504614', fax: '', categ: '2'},
		{ name: 'RSPAD Gatot Soebroto "Pav Darmawan" ', address: 'Jl. Abdul Rachman Saleh No.24', telphone: '3500004', fax: '', categ: '2'},
		{ name: 'RSPAD Gatot Soebroto "Pav Iman Sudjudi" ', address: 'Jl. Abdul Rachman Saleh No 24 ', telphone: '350494', fax: '', categ: '2'},
		{ name: 'RSPAD Gatot Soebroto "Pav Kartika" ', address: 'Jl. Abdul Rachman Saleh No 24', telphone: '3840484', fax: '', categ: '2'},
		{ name: 'Wellness Clinic', address: 'Jl. Lombok 38 Menteng', telphone: '31926822', fax: '', categ: '2'},

		#Jakarta Selatan
		{ name: 'Klinik Triaz ', address: 'Gedung Graha Niaga Lt.B-1 Jl. Jend. Sudirman 58', telphone: '250577', fax: '', categ: '3'},
		{ name: 'RS. Medistra ', address: 'Jl. Jen. Gatot Soebroto Kav. 49 Jakarta Selatan ', telphone: '5210200', fax: '5210185', categ: '3'},
		{ name: 'RS Tebet ', address: 'Jl. MT Haryono No.8 Tebet', telphone: '8307540', fax: '', categ: '3'},
		{ name: 'RS Budhi Jaya ', address: 'Jl. Dr Saharjo No 120 Jakarta Selatan', telphone: '8307540', fax: '', categ: '3'},
		{ name: 'RS Jakarta', address: 'Jl. Jend. Sudirman Kav 49 Jakarta', telphone: '5732241', fax: '', categ: '3'},
		{ name: 'RS. Tria Dipa ', address: 'Jl. Raya Pasar Minggu No. 3 A Pancoran', telphone: ' 	7993058', fax: '7974074', categ: '3'},
		{ name: 'RS. Pusat Pertamina ', address: ' 	Jl. Kyai Maja No. 43, Kebayoran Baru ', telphone: '7219204', fax: '7203540', categ: '3'},
		{ name: 'Semanggi Spesialist Clinic ', address: 'Plaza Semanggi Lt. 7 ', telphone: '25539326', fax: '25539325', categ: '3'},
		{ name: 'Sudirman Medical Center ', address: 'Jl. Jend. Sudirman Kav. 25 Jak - Sel ', telphone: '5203467', fax: '5203468', categ: '3'},
		{ name: 'Pertamina Medical Center ', address: 'Jl. Kyai Maja No. 43, Gedung H Lantai I, Kebayoran Baru ', telphone: '7219594', fax: '7219788', categ: '3'},
		{ name: 'Klinik Cinere Pertamina ', address: 'Jl. Karang Tengah No. 14 B Jak - Sel ', telphone: '75911106', fax: '', categ: '3'},
		{ name: 'Klinik Patra Jasa Pertamina', address: 'Jl. Jend Gat - Soe Kav. 32 - 34 Jak - Sel ', telphone: '5217318', fax: '', categ: '3'},
		{ name: 'Klinik Pakubuwono Pertamina ', address: 'Jl. Dempo No. 1 Jak - Sel', telphone: '7243223', fax: '', categ: '3'},
		{ name: 'RS. Siaga Raya ', address: 'Pejaten Barat Kav. 4 - 8, Pasar Minggu', telphone: '7972750', fax: '7970627', categ: '3'},
		{ name: 'RS. Pondok Indah', address: 'Jl. Metro Duta Kav. UE Pondok Indah', telphone: '7657525', fax: '75816567', categ: '3'},
		{ name: 'Klinik Spesialis "ESTI"', address: 'Jl. Bangka Raya No. 28, Jakarta ', telphone: '7194434', fax: '71791917', categ: '3'},
		{ name: 'RS. "AGUNG" Manggarai', address: 'Jl. Sultan Agung No. 67, Pasar Rumput, Manggarai, Jakarta Selatan', telphone: '8294955', fax: '8305791', categ: '3'},
		{ name: 'RSB. "ASIH" ', address: 'Jl. Panglima Polim No. 34 Jakarta Selatan ', telphone: '2700609', fax: '2700610', categ: '3'},
		{ name: 'RS. MMC ', address: 'Jl. H. R. Rasuna Said, Kav. C-21, Jakarta ', telphone: '5203435', fax: '5203417', categ: '3'},
		{ name: 'RS. "ZAHIRAH" ', address: 'Jl. Sirsak No. 21, Jagakarsa, Jakarta Selatan', telphone: '78888723', fax: '7872210', categ: '3'},
		{ name: 'RS. MATA Prof. DR. Isak Salim "AINI"', address: 'Jl. H. R. Rasuna Said, Kuningan, Jakarta Selatan', telphone: '5256228', fax: '5224288', categ: '3'},
		{ name: 'RSIA. "YADIKA" Kebayoran Lama ', address: 'Jl. Ciputat Raya No. 5, Kebayoran Lama, Jakarta Selatan 12240', telphone: '7291074', fax: '72895046', categ: '3'},
		{ name: 'Klinik "YADIKA" Petukangan', address: 'Jl. Damai Raya No. 1, Petukangan Selatan, Jakarta Selatan', telphone: '7370881', fax: '73887810', categ: '3'},
		{ name: 'RS. PRIKASIH ', address: 'Jl. Raya RS. Fastmawati No. 74, Pondok Labu, Cilandak ', telphone: '7501192', fax: '7505148', categ: '3'},
		{ name: 'RS. Hospital Cinere ', address: 'Jl. Maribaya No. 1 Puri Cinere ', telphone: '7455488 Ext. 1423', fax: '', categ: '3'},
		{ name: 'RS. Gandaria ', address: 'Jl. Gandaria Tengah II, No 12 - 14 Kebayoran Baru ', telphone: '7250718', fax: '7222038', categ: '3'},
		{ name: 'RS. Setia Mitra ', address: 'Jl. RS. Fatmawati No. 80-82 ', telphone: '7656000', fax: '7656875', categ: '3'},
		{ name: 'RS. Marinir Cilandak ', address: 'Jl. Cilandak KKO ', telphone: '7805415', fax: '7812764', categ: '3'},
		{ name: 'RS Khusus THT Ciranjang', address: 'Jl. Ciranjang No 22-22 Kebayoran Baru', telphone: '7243366', fax: '', categ: '3'},
		{ name: 'My Dentist Dental Clinic', address: 'Jl. Lamandau Raya No 18 Kebayoran Baru ', telphone: '7237903', fax: '', categ: '3'},
		{ name: 'Brawijaya Women and Children Hospital ', address: 'Jl. Taman Brawijaya No. 1 Cipete Utara ', telphone: '7211337', fax: '', categ: '3'},
		{ name: 'Easco Medical ', address: 'Century Tower Lt.1 Jl. HR Rasuna Said Kav. X2 No.4 Kuningan', telphone: '2523201', fax: '', categ: '3'},
		{ name: 'Banjarsari Orthodontist Clinic ', address: 'Jl. Banjarsari III/8 Cilandak Fatmawati Jakarta Selatan ', telphone: '7690980', fax: '', categ: '3'},
		{ name: 'RS Muhammadiyah Taman Puring ', address: 'Jl. Gandaria 1/20 Kebayoran Baru', telphone: '7250243', fax: '', categ: '3'},
		{ name: 'RSPAD Gatot Soebroto Askes dan Non Pav ', address: 'Jl. Abdul Rachman Saleh 24 ', telphone: '3441008', fax: '', categ: '3'},
		#Jakarta Timur
		{ name: 'RSIA. Hermina Jatinegara ', address: 'Jl. Jatinegara Barat No. 126 Jakarta Timur ', telphone: '8191223', fax: '8560601', categ: '4'},
		{ name: 'RS. Omni Medical Center ', address: 'Jl. Pulomas Barat VI No. 20 Jakarta 13210 ', telphone: '4723332', fax: '4718081', categ: '4'},
		{ name: 'RS. Islam Pondok Kopi ', address: 'Jl. Raya Pondok Kopi ', telphone: '8610471', fax: '86604256', categ: '4'},
		{ name: 'RS. F. K. UKI Cawang', address: 'Jl. May. Jen Soetoyo Cawang Jakarta Timur', telphone: '8092317', fax: '8092445', categ: '4'},
		{ name: 'RS. Mediros ', address: 'Jl. Perintis Kemerdekaan Kav. 149, Jakarta Timur ', telphone: '4750042', fax: '4891937', categ: '4'},
		{ name: 'RS. Harapan Jayakarta ', address: 'Jl. Bekasi Timur Raya Km. 18 No. 6, Pulogadung Jakarta Timur ', telphone: '4601371', fax: '4608863', categ: '4'},
		{ name: 'RS. Kartika Pulo Mas ', address: 'Jl. Pulomas Timur K No. 2, Jakarta Timur 13210', telphone: '4703333', fax: '4703333', categ: '4'},
		{ name: 'RS. Dharma Nugraha', address: 'Jl. Balai Pustaka Baru No. 19, Rawamangun, Jakarta Timur 13220', telphone: '4707433', fax: '4707428', categ: '4'},
		{ name: 'RS. MH. Thamrin Pondok Gede', address: 'Jl. Raya Pondok Gede No. 23 - 25, Kramat Jati', telphone: '8093602', fax: '8092235', categ: '4'},
		{ name: 'MH Thamrin UPK TEGALAN ', address: 'Jl. Tegalan No. 30', telphone: '8581344', fax: '', categ: '4'},
		{ name: 'Klinik Cipinang Cempedak Pertamina ', address: 'Jl. Cipinang Cempedak II No. 42 ', telphone: '8195194', fax: '', categ: '4'},
		{ name: 'Klinik Medan Satria Pertamina ', address: 'Jl. Arun IX No. 2 Jak - Tim', telphone: '4604944', fax: '', categ: '4'},
		{ name: 'Klinik Rawamangun Pertamina ', address: 'Jl. Mundu Raya No. 1 Jak Tim', telphone: '4894150', fax: '', categ: '4'},
		{ name: 'Klinik Harapan Sehat III ', address: 'Pulo Gadung Trade Center ', telphone: '46832748', fax: '', categ: '4'},
		{ name: 'RS. Mitra Internasional Jatinegara ', address: 'Jl. Raya Jatinegara Timur No. 87, Jakarta Timur ', telphone: '2800666', fax: '2800755', categ: '4'},
		{ name: 'RS. Harapan Bunda ', address: 'Jl. Raya Bogor Km. 22 No. 44, Jakarta Timur ', telphone: ' 	8400257', fax: '8412977', categ: '4'},
		{ name: 'RS. "HARUM" ', address: 'Jl. Tarum barat, Kalimalang, Jakarta Timur ', telphone: '8617212', fax: '8617213', categ: '4'},
		{ name: 'RS. "YADIKA" Pondok Bambu', address: 'Jl. Pahlawan Revolusi No. 47, Pondok Bambu, Jakarta Timur ', telphone: '8615754', fax: '8631708', categ: '4'},
		{ name: 'Klinik "YADIKA" Cibubur ', address: 'Jl. Lapangan Tembak No. 9, Pekayon, Cibubur, Jakarta Timur ', telphone: '8711515', fax: '', categ: '4'},
		{ name: 'RS. Haji Jakarta ', address: 'Jl. Raya Pondok Gede. Jakarta Timur ', telphone: '8000693', fax: '', categ: '4'},
		{ name: 'RSIA. Evasari ', address: 'Jl. Rawamangun No. 47 ', telphone: '4202851', fax: '4209725', categ: '4'},
		{ name: 'Klinik & RB Ibnu Sina ', address: 'Jl. Pendidikan Raya No. 33 Duren sawit Jakarta Timur 13440 ', telphone: '86603270', fax: '86601611', categ: '4'},
		{ name: 'Klinik Putewa ', address: 'Jl. Nusa Indah Raya Blok 40 No.17 H Malaka Raya - Jakarta Timur ', telphone: '8622207', fax: '', categ: '4'},
		{ name: 'RS Harapan Bunda ', address: 'Jl. Balai Pustaka Baru No. 29-31 Rawamangun ', telphone: '4893531', fax: '', categ: '4'},
		{ name: 'RS Bina Waluya ', address: 'Jl. TB Simatupang No. 71 Jakarta Timur ', telphone: '87781605', fax: '', categ: '4'},
		{ name: 'RS Bunda Aliyah ', address: 'Jl. Pahlawan Revolusi No.100 Pondok Bambu, Jakarta Timur', telphone: '86602525', fax: '', categ: '4'},
		{ name: 'Klinik Condet Jaya ', address: 'Jl. Condet Raya No 18 Jakarta Timur ', telphone: ' 	8090712', fax: '', categ: '4'},
		{ name: 'Klinik Suci Husada ', address: 'Jl. Suci No 21 RT 008 RW 06 Susukan Jakarta Timur', telphone: '87798796', fax: '', categ: '4'},
		{ name: 'RS Polri dr. Sukamto ', address: 'Jl. Raya Bogor Kramat Jati Jakarta Timur ', telphone: '8090559', fax: '', categ: '4'},
		{ name: 'RUSPAU Antariksa ', address: 'Jl. Merpati No.2 Lanud Halim Perdana Kusuma ', telphone: '80887296', fax: '', categ: '4'},
		
		
		#Jakarta Utara
		{ name: 'RSIA. Hermina Podomoro ', address: ' 	Jl. Danau Agung 2 Blok E3 ', telphone: '6404910', fax: '6518720', categ: '5'},
		{ name: 'RS. Pluit ', address: 'Jl. Raya Pluit Selatan No. 2 ', telphone: '6685006', fax: '6684878', categ: '5'},
		{ name: 'RS. Satya Negara Sunter ', address: 'Jl. Sunter Agung Utara Raya Blok A No. 1 ', telphone: '6852000', fax: '6518148', categ: '5'},
		{ name: 'RS. Mitra Keluarga Kemayoran ', address: 'Jl. Landas Pacu Timur Kemayoran Jakarta 10630 ', telphone: '6545555', fax: '6545959', categ: '5'},
		{ name: 'RS. Medika Griya Sunter Podomoro ', address: 'Jl. Danau Sunter Utara, Perumahan Nirwana Sunter Asri ', telphone: '6459877', fax: '6400778', categ: '5'},
		{ name: 'RS. Gading Pluit ', address: 'Jl. Boulevard Timur Raya, Kelapa Gading', telphone: '', fax: '', categ: '5'},
		{ name: 'Klinik Yos Sudarso Pertamina ', address: 'Jl. Yos Sudarso No. 34', telphone: '4353460', fax: '', categ: '5'},
		{ name: 'Klinik Deli Pertamina ', address: 'Jl. Deli No. 22', telphone: '4301031 Ext. 3348', fax: '', categ: '5'},
		{ name: 'RS. Pantai Indah Kapuk ', address: 'Jl. Pantai Indah Utara No. 3, Pantai Indah Kapuk, 14460 ', telphone: '5880911', fax: '5881414', categ: '5'},
		{ name: 'RS. Mitra Keluarga Kelapa Gading ', address: 'Jl. Bukit Gading Raya Kav. 2, Kelapa Gading Permai ', telphone: '45852700', fax: '', categ: '5'},
		{ name: 'RS. Sukmul ', address: 'Jl. Tawes No. 18-20 Tanjung Priok ', telphone: '4301269', fax: '4301272', categ: '5'},
		{ name: 'RS Port Medical Center ', address: 'Jl. Enggano No.10 Tanjung Priok', telphone: '43902350', fax: '', categ: '5'},
		{ name: 'Klinik Delarosa', address: 'Club House 1st Floor, Apartement Gading Resort Residences Kelapa Gading Square Jl. Raya Boulevard Barat', telphone: '45866792', fax: '', categ: '5'},
		{ name: 'RS Family ', address: 'Jl. Pluit Mas I No. 2A - 5A', telphone: '6695066', fax: '', categ: '5'}
	])


