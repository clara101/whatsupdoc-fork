class AddColumnToAppointment < ActiveRecord::Migration
  def change
  	add_column :appointments, :new_patient, :boolean
  	add_column :appointments, :appointment_datetime, :datetime
  	add_column :appointments, :doctor_id, :integer
  end
end
