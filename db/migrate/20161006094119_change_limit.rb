class ChangeLimit < ActiveRecord::Migration
  def change
  	change_column :users, :provider, :string, :limit => 255
  	change_column :users, :name, :string, :limit => 255
  	change_column :users, :oauth_token, :string, :limit => 255
  end
end
