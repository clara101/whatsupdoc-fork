class CreateHospitals < ActiveRecord::Migration
  def change
    create_table :hospitals do |t|
      t.string :name
      t.text :address
      t.string :telphone
      t.string :fax

      t.timestamps null: false
    end
  end
end
