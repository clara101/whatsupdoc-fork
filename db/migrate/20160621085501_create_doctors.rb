class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :first_name
      t.string :last_name
      t.string :specialty
      t.string :email
      t.string :mobile_number
      t.integer :zipcode
      t.string :education
      t.text :hospital_affiliations
      t.string :language_spoken
      t.string :certification
      t.string :proffesional_membership
      t.text :proffesional_statement
      t.string :in_network_insurance

      t.timestamps null: false
    end
  end
end
